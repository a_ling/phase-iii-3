package com.lagou.service;

public interface MyService {
    public String methodA(String name);

    public String methodB(String name);

    public String methodC(String name);
}
