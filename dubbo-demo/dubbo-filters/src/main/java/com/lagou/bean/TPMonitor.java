package com.lagou.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : liuchangling
 * @Descrition :
 * @Date： Created in 4:50 下午 2021/12/27
 */
public class TPMonitor {
    //请求方法
    private String methodName;
    //运行时间
    private long runTime;
    //结束时间
    private long endTime;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public long getRunTime() {
        return runTime;
    }

    public void setRunTime(long runTime) {
        this.runTime = runTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
