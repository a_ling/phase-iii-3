package com.lagou.service.impl;

import com.lagou.service.MyService;

import java.util.concurrent.ThreadLocalRandom;

public class MyServiceImpl implements MyService {
    @Override
    public String methodA(String name) {
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("method A: " + name);
        return "method A: " + name;
    }

    @Override
    public String methodB(String name) {
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("method B: " + name);
        return "method B: " + name;
    }

    @Override
    public String methodC(String name) {
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("method C " + name);
        return "method C: " + name;
    }
}
