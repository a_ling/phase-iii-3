package com.lagou.bean;

import com.lagou.service.MyService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

@Component
public class ConsumerComponent {
    @Reference
    private MyService myService;

    public String methodA() {
        return myService.methodA("hello");
    }

    public String methodB() {
        return myService.methodB("hello");
    }

    public String methodC() {
        return myService.methodC("hello");
    }
}
