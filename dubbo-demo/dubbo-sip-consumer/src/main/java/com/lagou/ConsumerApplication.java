package com.lagou;

import com.lagou.bean.ConsumerComponent;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.concurrent.*;

/**
 * @Author : liuchangling
 * @Descrition :
 * @Date： Created in 3:08 下午 2021/12/27
 */
public class ConsumerApplication {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfig.class);
        context.start();

        ConsumerComponent consumerComponent = context.getBean(ConsumerComponent.class);

        long startTime = System.currentTimeMillis();
        ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(100,100,0L, TimeUnit.SECONDS,new ArrayBlockingQueue<>(50));
        while (true){
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            threadPoolExecutor.execute(()->{
                consumerComponent.methodA();
            });
            threadPoolExecutor.execute(()->{
                consumerComponent.methodB();
            });
            threadPoolExecutor.execute(()->{
                consumerComponent.methodC();
            });
            if ((System.currentTimeMillis() - startTime) > 0.1 * 60 * 1000) break;
        }


    }

    @Configuration
    @EnableDubbo
    @PropertySource(value = "classpath:/dubbo-consumer.properties")
    @ComponentScan(basePackages = {"com.lagou.bean"})
    static class ConsumerConfig {

    }
}
